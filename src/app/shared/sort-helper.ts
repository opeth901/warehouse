import { SortKey } from './sort-key';

export class SortHelper
{
    SortByKeys(list, keys: SortKey[])
    {
        list = list.sort(this.CompareByKyes(keys));
    }

    CompareByKyes(keys: SortKey[])
    {
      const self = this;
      return function innerCompare(first: object, second: object) {
          for(const key of keys)
          {
            const compareRes = self.CompareBySingleKey(first, second, key.Key, key.Order);

            if(compareRes !== 0)
            {
              return compareRes;
            }
          }
          return 0;
      };
    }

    CompareBySingleKey(first: object, second: object, key, order)
    {
      if (this.CheckIfPropertyExists(first, second, key))
      {
        return 0;
      }
      const valueA = this.ConvertValue(first, key);
      const valueB = this.ConvertValue(second, key);
      const comparison = this.CompareValues(valueA, valueB);
      return this.ApplyOrderDirection(comparison, order);
    }

    CompareValues(valueA, valueB)
    {
      let comparison = 0;
      if (valueA > valueB)
      {
        comparison = 1;
      }
      else if (valueA < valueB)
      {
        comparison = -1;
      }
      return comparison;
    }

    CheckIfPropertyExists(first,second,key)
    {
      return !first.hasOwnProperty(key) || !second.hasOwnProperty(key);
    }

    ConvertValue(obj,key)
    {
        return (typeof obj[key] === 'string') ? obj[key].toUpperCase() : obj[key];
    }

    ApplyOrderDirection(comparison, order)
    {
      return ((order === 'desc') ? (comparison * -1) : comparison);
    }
}
