export class SortKey
{
    Key: string;
    Order: string;
    constructor(key: string, order: string)
    {
        this.Key = key;
        this.Order = order;
    }
}
