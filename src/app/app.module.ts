import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataLoaderComponent } from './warehouse/loader.component';
import { FileLoaderComponent } from './warehouse/file-loader.component';
import { TextLoaderComponent } from './warehouse/text-loader.component';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { WarehouseService } from './warehouse/shared/warehouse.service';
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    AppComponent,
    DataLoaderComponent,
    FileLoaderComponent,
    TextLoaderComponent,
    WarehouseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatInputModule,
    MatCardModule,
    StoreModule.forRoot({}, {})
  ],
  providers: [WarehouseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
