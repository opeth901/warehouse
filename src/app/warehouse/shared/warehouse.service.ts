import { Injectable } from '@angular/core';
import { WarehouseModel } from './warehouse.model';
import { Subject } from 'rxjs';
import { SortHelper } from '../../shared/sort-helper';
import { SortKey } from 'src/app/shared/sort-key';


@Injectable()
export class WarehouseService
{
    private splitLinesSign = '\n';
    private splitMaterialSign = ';';
    private splitWarehouseSign = '|';
    private splitQuantitySign = ',';
    private commentSign = '#';

    private warehouseStateList = new Subject<WarehouseModel[]>();
    Warehouses = this.warehouseStateList.asObservable();

    private warehouseList: WarehouseModel[] = [];
    private sorter = new SortHelper();

    public ProccessText(text: string): void
    {
        const lines = text.split(this.splitLinesSign);
        for(const line of lines)
        {
            if(this.ValidateLine(line) === false)
            {
                continue;
            }
            this.ProccessLine(line);
        }

        this.SortWarehousesData();
        this.NotifyWarehouseUpdate();
    }

    private SortWarehousesData(): void
    {
        this.sorter.SortByKeys(this.warehouseList,[new SortKey('TotalQuantity', 'desc'), new SortKey('Name', 'desc')]);
        for(const warehouse of this.warehouseList)
        {
            this.sorter.SortByKeys(warehouse.List, [new SortKey('MaterialId', 'asc')]);
        }
    }

    private ValidateLine(line: string): boolean
    {
        if(line[0] === this.commentSign)
        {
            return false;
        }

        if(line.split(this.splitMaterialSign).length !== 3)
        {
            return false;
        }

        return true;
    }

    private ProccessLine(line: string): void
    {
        const lineParts = line.split(this.splitMaterialSign);
        if(lineParts.length === 3)
        {
            const materialName = lineParts[0];
            const materialId = lineParts[1];
            const warehousesWithQuantities = lineParts[2].split(this.splitWarehouseSign);
            for(const warehouseEntry of warehousesWithQuantities){
                this.ProccessWarehouse(warehouseEntry, materialName, materialId);
            }
        }
    }

    private ProccessWarehouse(warehouseEntry: string, materialName: string, materialId: string): void
    {
        const warehouseEntryParts = warehouseEntry.split(this.splitQuantitySign);
        if(warehouseEntryParts.length === 2)
        {
            const name = warehouseEntryParts[0];
            const quantity = warehouseEntryParts[1];
            let warehouse = this.GetWarehouse(name);
            if(warehouse === undefined)
            {
                warehouse = new WarehouseModel(name);
                this.warehouseList.push(warehouse);
            }
            warehouse.UpdateInventory(materialName, materialId , parseInt(quantity, 10));
        }
    }

    private GetWarehouse(name: string): WarehouseModel
    {
        return this.warehouseList.find(x =>
        {
            return x.Name === name;
        });
    }

    private NotifyWarehouseUpdate(): void
    {
        this.warehouseStateList.next(this.warehouseList);
    }

    ClearWarehouse(): void
    {
        this.warehouseList = [];
        this.NotifyWarehouseUpdate();
    }
}
