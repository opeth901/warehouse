export class MaterialEntryModel
{
    public MaterialId: string;
    public MaterialName: string;
    public Quantity: number;

    constructor(materialName: string, materialId: string,  quantity: number)
    {
        this.MaterialName = materialName;
        this.MaterialId = materialId;
        this.Quantity = quantity;
    }
}
