import { MaterialEntryModel } from './material-entry.model';

export class WarehouseModel
{
    public Name: string;
    public TotalQuantity: number;
    public List: MaterialEntryModel[];

    constructor(name: string)
    {
        this.List = [];
        this.Name = name;
        this.TotalQuantity = 0;
    }

    public UpdateInventory(materialName: string, materialId: string, quantity: number): void
    {
        const entry = this.GetMaterialIfExists(materialName);
        if(entry === undefined)
        {
            this.AddEntry(materialName, materialId, quantity);
        }
        else
        {
            this.UpdateQuantity(entry, quantity);
        }
        this.UpdateTotalQuantity(quantity);
    }

    private AddEntry(materialName: string, materialId: string, quantity: number): void
    {
        this.List.push(new MaterialEntryModel(materialName, materialId, quantity));
    }

    private GetMaterialIfExists(materialName: string): MaterialEntryModel
    {
        return this.List.find(x=> x.MaterialName === materialName);
    }

    private UpdateQuantity(entry: MaterialEntryModel, quantity: number): void
    {
        entry.Quantity += quantity;
    }

    private UpdateTotalQuantity(quantity: number): void
    {
        this.TotalQuantity += quantity;
    }
}
