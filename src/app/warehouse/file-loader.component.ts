import { Component } from '@angular/core';
import { WarehouseService } from 'src/app/warehouse/shared/warehouse.service';

@Component({
  selector: 'app-file-loader',
  templateUrl: './file-loader.component.html'
})
export class FileLoaderComponent  {

  constructor(private warehouseService: WarehouseService)
  {

  }

  public onChange(fileList: FileList): void {
      const file = fileList[0];
      const fileReader: FileReader = new FileReader();
      const self = this;
      fileReader.onloadend = (x) => {
          self.warehouseService.ProccessText(fileReader.result as string);
          clearFileInput();
      };
      fileReader.readAsText(file);
  }
}

function clearFileInput(){
  (document.getElementById('fileUpload') as HTMLInputElement).value = '';
}
