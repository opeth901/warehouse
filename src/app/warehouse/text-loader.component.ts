import { Component } from '@angular/core';
import { WarehouseService } from 'src/app/warehouse/shared/warehouse.service';

@Component({
  selector: 'app-text-loader',
  templateUrl: './text-loader.component.html'
})
export class TextLoaderComponent  {

  constructor(private warehouseService: WarehouseService)
  {

  }

  public onChange(textInput: string): void
  {
    this.warehouseService.ProccessText(textInput);
  }
}
