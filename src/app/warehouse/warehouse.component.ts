import { Component, OnInit } from '@angular/core';
import { WarehouseService } from 'src/app/warehouse/shared/warehouse.service';
import { WarehouseModel } from './shared/warehouse.model';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html'
})
export class WarehouseComponent implements OnInit
{
    WarehouseList: WarehouseModel[];

    constructor(private warehouseService: WarehouseService)
    {
    }

    ngOnInit()
    {
        this.warehouseService.Warehouses.subscribe(s =>
            {
                this.WarehouseList = s;
            });
    }

    ClearWarehouse(): void
    {
        this.warehouseService.ClearWarehouse();
    }

    ShowClearButton(): boolean
    {
        return this.WarehouseList !== undefined && this.WarehouseList.length > 0;
    }
}
